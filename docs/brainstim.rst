.. This is a comment. Note how any initial comments are moved by
   transforms to after the document title, subtitle, and docinfo.

======================
Brain Stimulation Lab
======================

Lab head: `Pr Elsa Fouragnan <https://www.plymouth.ac.uk/staff/elsa-fouragnan>`__
Lab manager: Joshua Marquez

Non-invasive brain stimulation (NIBS) techniques aim to modulate the function of specific brain regions with the intention of modifying cognitive and behavioural performance in a safe manner. The ones used in the Brain Stimulation Lab at BRIC include :ref:`Transcranial Ultrasound Stimulation <tus>`, :ref:`Transcranial Magnetic Stimulation <tms>`, and :ref:`Transcranial Electric Stimulation <tes>`. 

.. include:: /brainstim/Lab_Resources.rst
