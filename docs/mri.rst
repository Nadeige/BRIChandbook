.. _MRI_general:

================
MRI Lab general
================

_`MRI lab heads`: `Dr Nadège Bault <https://www.plymouth.ac.uk/staff/nad_ge-bault>`__ & Matt Roser `Dr Matt Roser <https://www.plymouth.ac.uk/staff/matt-roser>`__

You can reach them both using this `email address <BRIC.MRI-lab.heads@plymouth.ac.uk>`_

_`Lead radiographer`: Ben Walton

_`MR physicists`: Jamie Roberts and Malek Benattayallah

.. figure:: /mri/Prisma_Pic.jpg
   :width: 300px
   :align: right

Lab resources
-------------

Information about our MRI scanner the Siemens Magneton Prisma 3T can be found `here <https://www.siemens-healthineers.com/en-uk/magnetic-resonance-imaging/3t-mri-scanner/magnetom-prisma>`__

We have the following kits available in the Lab

- stimulus presentation computer, screen, responses buttons and a joystick for task based fMRI research (see :doc:`/mri/peripherals` for details)
- MR compatible biopac with EDA and ECG electrodes
- MR compatible glasses with a range of prescription lenses

We have a number of ready-to-go MR a sequences for

- BOLD research, 
- DTI and 
- Spectroscopy

Planning a new study - step by step
-------------------------------------------

1. Fill a Proof of concept form or a funded study form
2. Get ethics approval

Once your Proof of Concept / pump priming application is approved:

3. Contact the `Lead radiographer`_ to get safety trained (level1) or the `MRI lab heads`_ if you need advanced training. You will only get free access to the lab after you have logged about 10 hours in the lab and demonstrated that you understand and comply to the safety rules.
4. Ask the `MRI lab heads`_ to give you access to the MRI booking system
5. Make an appointment with the MRI lab heads and/or `MR physicists`_ to set your protocole up, on the scanner console

Once your ethics is approved:

6. Book the scanner to run a phantom scan and one or two pilots. Once your data is sent to the cloud, this will automatically create a folder with your study ID
7. Ask the :ref:`System Administrator <sysadmin>` to create a storage for your study.
8. Check your pilot data. Ideally test your analysis pipeline as well
9. You are ready to start your data acquisition.

Safety and ethics
------------------
See these documents for details of BRIC MR Safety and Operating rules and policies.

:download:`BRIC MRI Local Rules <mri/BRIC_MRI_Local_Rules-12.09.21.pdf>`

:download:`BRIC Safety and Risk Operational Policy <mri/BRIC_Safety_and_Risk_Operational_Policy-12.09.21.pdf>`


.. _safety:

Safety training
~~~~~~~~~~~~~~~

The levels of safety training and access to the scanner are summarised in the :download:`MR lab access levels document <mri/MRIlab_accesslevels.pdf>`

Before being able to access the MRI lab, you will need to complete a safety training. Safety training sessions may be ran monthly or bi-monthly, depending on the needs, in small groups. The training will be renewed annually. For level1, this annual refresher will be online.

Safety questionnaires must be filled by staff and research participants before they can enter the restricted MRI environment. 

:download:`safety questionnaire - adult research participants <mri/BRIC MRI safety Adults questionnaire - research participants.pdf>`

:download:`safety questionnaire - staff (permit to work) <mri/BRIC MRI safety questionnaire - Staff (permit to work).pdf>`

Ethics
~~~~~~

You can find template information and consent forms in section :doc:`/mri/mrihowtos` :ref:`mriethics`, as well as a general information sheet for participants comming for a scan.