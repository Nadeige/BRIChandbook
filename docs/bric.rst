========================
BRIC general information
========================

.. meta::
   :description lang=en: Discover the labs at BRIC and find some information on how make the best of them.

The `Brain Research & Imaging Centre (BRIC) <https://www.plymouth.ac.uk/facilities/brain-research-imaging-centre>`__ is the most advanced multi-modal brain research facility in the South West and represents a sea-change in research capability in the field of human neuroscience.

Contact information
-----------------------------------

_`BRIC director`: `Stephen Hall <https://www.plymouth.ac.uk/staff/stephen-hall>`__

_`Research Administrator`: `Kathryn Callicott <kathryn.callicott@plymouth.ac.uk>`__

.. _sysadmin:

_`System Administrator`: `Paul Greening <paul.greening@plymouth.ac.uk>`__

""""""""""""""""""

Access to BRIC
~~~~~~~~~~~~~~~

.. figure:: /_static/bric_xlarge_2100102.jpg
   :width: 300px
   :align: right

   BRIC building

Address: Research Way, Plymouth Science Park, Plymouth, PL6 8BU

* By bus: Bus 51, stop at Miller way & Research way. 
* By car: A limited number of parking space is available.

Map and BRIC entrance:

.. figure:: /_static/BRIC_Map_and_Entrance_photo.jpg
   :width: 600px
   :align: center

""""""""""""""""""

BRIC anonymous participant ID
-----------------------------------

.. admonition:: BRIC anonymous participant ID

   You can generate the BRIC unique participant codes used to store data anonymously by following this `link <https://bric-plymouth.gitlab.io/bric-id-generator/>`__

