=======================
Psychopharmacology Lab
=======================

Lab head: `Dr Sean Fallon <https://www.plymouth.ac.uk/staff/sean-fallon>`__


 `The Psychopharmacology Laboratory <https://www.plymouth.ac.uk/facilities/psychopharmacology-laboratory>`__ works to understand the factors that affect individual variation in response to drugs.

Lab Resources
-------------

- Pharmacology
- Pain model
- Biopac with ECG and EDA recording kits
- O2 and anxiety CO2 model