.. _MRI_how_to:

==============
MRI Lab how to
==============

.. _mriethics:

Get ethics approval
-------------------

Please follow `this link <https://www.plymouth.ac.uk/research/governance/research-ethics-policy>`_  to get the general information you need to submit an ethics application at the University of Plymouth.

Template for consent and information forms can be found below.

Information & consent template forms
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _templates:

Template information form in
:download:`.odt <Information_form.odt>` and 
:download:`.docx <Information_form.docx>`

Template consent form in 
:download:`.odt <Consent_form.odt>` and 
:download:`.docx <Consent_form.docx>`

General information for participants
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This :download:`general information form <General_Information_MRI-lab.docx>` contains information for patients/participants about the MRI environment, how to prepare for a session and how to come to BRIC. Please add your contact information at the end of the form before sending it to your participants!

Pre-screen participants
-----------------------

The radiographers or MR operators will screen your participants for safety on the day of scanning. However, to decrease the rejection rate on the day of scanning, researchers should pre-screen their participants. To do so, researchers can send the safety questionnaire to the prospective participant and ask them to fill it.
If the participant answers 'yes' to any of the exclusion question, the researcher should make sure the participant is confortable about discussing their answer before asking for more details. In case of doubt, please contact the MRI lab heads. 

:download:`safety questionnaire - adults research participants <BRIC MRI safety Adults questionnaire - research participants.pdf>`


Book the scanner
----------------

`Booking system <https://resourcebooker.plymouth.ac.uk>`_  for University researchers. Once your project has been approved, please contact the MRI lab heads to be granted access as an MRI booker.

Retrieve your MR images
-----------------------

All research scans are being sent to our Orthanc server on Azure, then converted from dicom (.dcm) to nifti (.nii). The PI of the study can ask to get read access to this cloud storage for all the collaborators of the study. Please contact our :ref:`system administrator <sysadmin>` to request access. 