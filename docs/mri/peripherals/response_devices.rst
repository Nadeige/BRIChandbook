.. _response_devices:

Response device instructions and notes
--------------------------------------

Response devices input optical signals from the MRI room to the FIU-932Universal fORP Interface in the control room.

The fORP box receives button/joystick responses, as well as the optical trigger sent by the scanner at the start of each volume acquisition

The fORP box sends button presses and the scanner trigger as ascii characters to the presentation desktop computer.
The ascii character corresponding to the scanner trigger is "t".

More information on using the fORP932 is available here:
:download:`Getting Started fORP932 </mri/peripherals/Getting_started_FORP932.pdf>`
and on this `manufacturer website <https://wiki.curdes.com/bin/view/CdiDocs/932QuickSetup>`__


Button box
~~~~~~~~~~

For studies using the button boxes, the fORP box should be set as shown in the photo below. Check that the display is not flashing. This can happen if the experimental machine goes to sleep. Restart the box if flashing.

.. figure:: /mri/peripherals/932_fORP_for_MRI.jpg
   :alt: FIU-932Universal fORP Interface
   :width: 500px
   :align: center

   FIU-932Universal fORP Interface

The two rows of LEDs on the fORP box show incoming responses using the buttons. The LED at the bottom of the rightmost pair shows the scanner trigger. The top is power.

The buttons output ascii characters via USB to the stimulus presentation computer. The table below shows the mapping of characters to buttons:

.. figure:: /mri/peripherals/Buttons_2x4_with_legend.png
   :alt: HHSC-2x4-C response buttons
   :width: 150px
   :align: right


.. list-table:: Ascii characters output by button boxes
   :widths: 40 25 25 25 25
   :header-rows: 0

   * - **Button colour**
     - blue
     - yellow
     - green
     - red
   * - **Right hand**
     - b
     - y
     - g
     - r
   * - **Left hand**
     - e
     - w
     - n
     - d
   * - **Finger**
     - index
     - middle
     - ring
     - pinky


Joystick
~~~~~~~~

For studies using the Joystick, the fORP box should be set according to these available options. Use the right wheel on the fORP box to scroll.

:HD Joystick: buttons act as mouse click.
:Joystick Mouse: works as a mouse with click.
:HID Joy Comp: non-responsive.

