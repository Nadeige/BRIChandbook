.. _Peripherals:

=========================================
MRI Peripherals and Experimental Software
=========================================


Overview
--------

We have a range of peripherals that allow researchers to conduct psychological and cognitive neuroscience experiments in the MRI scanner.


Stimulus presentation
~~~~~~~~~~~~~~~~~~~~~

**Cambridge Research BOLDScreen 32**

This 1920x1080 @120 Hz screen is positioned at the top of the bore and can present visual stimuli which are viewable in the scanner using the mirror attachment.

The Getting Started guide is available here.
:download:`BOLDScreen32_Getting_Started </mri/peripherals/071-CRS-0154_BOLDscreen32AVI_Getting_Started_Guide.pdf>`

Find more information on how to use the screen here: :ref:`screen`.

**Cambridge Research BOLDfonic Audio System**

This system allows audio delivery, trigger control and synhronisation with the BOLDScreen. It includes two earphone/headphone options:

- Ultra-thin piezo-electric headphones (HP PI US 03). These headphones work both inside and outside the MRI room.
- Electrodynamic insert headphones (HP AT 01). These earphones only work within the scanner's magnetic field.

.. figure:: /mri/peripherals/HP_PI_US_03_headphones.png
   :alt: HP PI US 03 piezo-electric headhpones
   :width: 350px
   :align: left

   HP PI US 03 piezo-electric headphones

.. figure:: /mri/peripherals/HP_AT_01_earphones.png
   :alt: HP AT 01 electrodynamic earphones
   :width: 275px
   :align: left

   HP AT 01 electrodynamic earphones


Find more information on how to use the audio system here: :ref:`sound_presentation`.


Response devices
~~~~~~~~~~~~~~~~

We currently have two options to record participants' manual responses in the scanner

- a set of HHSC-2x4-C response buttons. One is labelled (on its back) for use in the right hand. 
- a HHSC-JOY-5 Tethyx Joystick

.. figure:: /mri/peripherals/Buttons_2x4.jpg
   :alt: HHSC-2x4-C response buttons.
   :width: 350px
   :align: left

   HHSC-2x4-C response buttons (left and right).

.. figure:: /mri/peripherals/Tethyx_Joystick.jpg
   :alt: HHSC-JOY-5 Tethyx Joystick
   :width: 275px
   :align: left

   HHSC-JOY-5 Tethyx Joystick

Find more information on how to set up the response devices below (:ref:`response_devices`).


Experimental software
~~~~~~~~~~~~~~~~~~~~~

In the MRI control room is a Windows desktop computer running experimental software including:

- `PsychoPy <https://psychopy.org/>`_
- `Presentation <https://www.neurobs.com/>`_
- Matlab `Psychtoolbox <http://psychtoolbox.org/>`_
- `OpenSesame <https://osdoc.cogsci.nl/>`_ (We do not recommend the use of OpenSesame for time-critical designs. OpenSesame is not suitable as the timing accuracy is too low.)

The computer receives input from the response devices and scanner as **keyboard (or mouse) events**. You can use these to record responses and to record or sync to the scanner trigger.

Detailed guides
----------------

.. include:: /mri/peripherals/response_devices.rst
.. include:: /mri/peripherals/screen.rst
.. include:: /mri/peripherals/sound_presentation.rst
