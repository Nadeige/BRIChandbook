.. _tus:

===
TUS
===

Most information will be stated in the **NIBS Manual** with a more in-depth look at the steps and techniques. 
Please view :ref:`Forms and Documentation <forms>` for the **NIBS lab**.
**For the latest version of the NIBS Manual, please contact the Head of the lab.**


Starting a new study
--------------------

Prescreening 
~~~~~~~~~~~~

The TUS Safety Questionnaire must be filled in by research participants before they can undergo TUS. Once your participant has filled it in, it is your responsibility to go through the questions carefully with them to check that they are TUS safe. 

.. Important::
   Please take notice of these requirements when recruiting participants:
   **Neurological Conditions**;
   **Previous Head Traumas**;
   **Piercings and Tattoos**;
   **Medication**;
   **Any metals inside the body**;

Verify with a supervisor before accepting participant.

Please view the TUS screening safety here :download:`TUS screening safety questionnaire </brainstim/BRIC_TUS_safety_questionnaire.pdf>`

.. _Developing a task:

Developing a task
~~~~~~~~~~~~~~~~~

Developing a task can be done using the following material:

   - `Matlab: Psychtoolbox <http://psychtoolbox.org/>`__
   - `Presentation: Neurobs <https://www.neurobs.com/menu_presentation/menu_features/features_overview>`__

To renew license for **Presentation**, please contact the Head of the NIBS lab.
All the material above can be used with the :ref:`Lab Resources <computers>`

.. _Booking a room:

Booking a room 
~~~~~~~~~~~~~~

To book the TUS room, please follow the same procedure as the other BRIC rooms by directing yourself to the 
`Resource Booker <https://resourcebooker.plymouth.ac.uk/>`__ site.
Please contact the NIBS lab head or the lab manager to get access

.. _Ethics:

Ethics
------

- Ethics: ensure you have ethical approvals in place before you start your study.

- Read the Brain Stimulation Safety Report for policies and rules on all non-invasive brain stimulation studies (TUS, TMS, tES).

:download:`Brain Stimulation Safety Report </brainstim/Brain_Stimulation_Safety_Report_v12.pdf>`

To apply for ethics view `Ethics Online <https://apply-ethicsonlinesystem.plymouth.ac.uk/>`__.


.. _General information for participants:

General information for participants
------------------------------------

This document contains some information about TUS that you may wish to use in your participant information sheet.

:download:`Sample information TUS </brainstim/Sample_information_TUS.pdf>`

.. _TUS_MRI:

Simultaneous TUS-MRI
~~~~~~~~~~~~~~~~~~~~

At the current moment of time, there is not simultaneous TUS and MRI stimulations in the lab.

.. _TUS_EEG:

Simultaneous TUS-EEG
~~~~~~~~~~~~~~~~~~~~

Please contact the EEG and NIBS lab heads for information 

.. _Quality assessment:

Quality assessment
------------------

.. _RF_Wattmeter:

RF-Wattmeter
~~~~~~~~~~~~

The Radio-Frequency Wattmeter is a measurement used in the lab to ensure the efficiency and frequency of the TUS system. This can be found under [COMPARTMENT] and is further explained on the :ref:`NIBS Manual <forms>`. 

When conducting the Quality Check, please ensure that the TPO cables are of the correct order, and that the TUS parameters have been changed when conducting with the RF and after the test is done (View the :ref:`NIBS Manual <forms>` for more information). 

.. :note:: 
   In case of doubt, a **physical copy of the RF Wattmeter manual** is available inside the box with the RF Wattmeter equipment.

.. _Running a study:

Running a study
---------------

.. _t1w:

Acquiring a T1w and PETRA
~~~~~~~~~~~~~~~~~~~~~~~~~

Before doing the first session of your TUS study, it is mandatory to acquire a T1-weighted magnetic resonance (T1w MR) image of your participant. This image will be used to guide the stimulation during the TUS sessions. 
During the MRI sessions, make sure to gather the PETRA scan, as it will then be used to create the *pCT*. We want such images to allow us to make better inferences on the skull imaging (Please consult the section 4.1.2.2 of the :ref:`NIBS Manual <forms>` for more information on how to do the pCT conversion.)


Information on how to perform an MRI session at BRIC is available here: :ref:`MRI LAB <MRI_general>`

.. _Calibration:

Calibration
~~~~~~~~~~~

Before your participant arrives, one of the most important tasks is to make sure the transducer and participant are seen accurately in virtual space. To do this, place the fiducial markers on the transducer, and fit the other fiducial markers on either the headband or glasses. Use the calibration block and ensure the transducer is stable and then use Brainsight to capture the fiducials. (View the TUS MANUAL for more detailed information on how to operate this task) 

.. figure:: /brainstim/calibration_blocks.jpg
   :alt: Calibration blocks used with the different tracker configurations. Left: ... Right: White calibration block for calibration set-up 2 (view manual). The offset is at 16.4. 
   :width: 350px
   :align: left

   Calibration blocks used with the different tracker configurations. Left: Black calibration block for calibration set-up 1   Right: White calibration block for calibration set-up 2

Different set-ups may be needed during different studies.  The main difference between these set-ups is the **OFFSET** when calibrating. These set-ups are also mounted differently. Please View the :ref:`NIBS Manual <forms>` for more information on how to mount each set-up.


.. _Brainsight setup:

Brainsight set-up
~~~~~~~~~~~~~~~~~

When using the Neuronavigation system, use BrainSight to create/ open a project. This software will be used to locate the brain regions for stimulation. This system will then be used together with the fiducial markers and fiducial "gun pointer" to track the participant through space.
To view how to create a Brainsight project please direct to :ref:`Forms and Documentation <forms>` for the TUS Manual.
To view more information on the fiducial equipment please direct to :ref:`Lab Resources <LabResources>`.

.. _Acoustic stimulation:

Acoustic stimulation
~~~~~~~~~~~~~~~~~~~~

To run the acoustic simulations, please contact Elsa Fouragnan. (Also, view the :ref:`NIBS Manual <forms>` for more information) 

For a general use, this tool can be used to calculate Ultrasound Intensity (**Please use as a general guide rather than a primary source of information**): `TUS calculator <https://www.socsci.ru.nl/fusinitiative/tuscalculator/>`__

.. _Running TUS:

Running TUS
~~~~~~~~~~~

.. Important::
   **Please note that the transducer MUST stay under water or against a participants' head before pressing the TPO button!**
   
   
.. _Double blindness:

Double blindness
~~~~~~~~~~~~~~~~

The participant must be unaware of the experimental condition (sham: fake stimulation, or TUS), as well as the experimenter who will be involved in data management, collection and analysis, to reduce bias and difference in treatment (View the :ref:`NIBS Manual <forms>` 3.3.3 to see how it is implemented). 
Make sure to blind participants and the experimenter from the TPO.

.. Note::
   You can prepare the TPO prior to the session. During the explanation of a task or the experimenter and participant leaving the room, switch off the TPO at the back. You can also switch the extension plug with your foot (If done however, please hide extension from the participant and experimenters vision).
   Alternatively, you can mask the sound of switching off the TPO.


Tips for Double Blinding proceedures
- Memorise the depth regardless, and create samples to mask any potential belief the experimenter might have when conducting the stimulation
- Make sure to cover the TPO correctly so that the experimenter is not aware of the condition

.. _sound:

Sound Masking
~~~~~~~~~~~~~

Depending on the study and its protocol, sound masking will be needed for SHAM stimulations.
This will be done using the *Participant Conductors* which connect via **bluetooth**.
Please contact supervisor to allow discussion on which sound masking audio will be used for the study. 


For the experimenter who will conduct the stimulation, please use Noise cancelling headphones, or MRI yellow plugs to block any further distinguished sound (Please view the equipment under :ref:`Lab Resources <LabResources>`)


.. Note::
   Make sure to always have the same sound volume for each participant. **As a tip, you can download the sound masking audio and place your phone on flight mode to avoid messages.**
   Verify the headphones are prepared **prior** to the session starts.

Tips for Sound Masking proceedures
- Memorise the volume of the headphones and maintain that volume for each participant.
- Ensure no sound continues after the stimulation stoppage.

.. _Post TUS questionnaires:

Post TUS questionnaire
~~~~~~~~~~~~~~~~~~~~~~

Our policy is to ask participants to report any symptoms they think may be associated with TUS after each session and one month after their final session. This is currently done via JATOS. Please ask Nadège to be added to the study.

`Brain stimulation report of symptoms (NIBS lab) <https://jatos.plymouth.ac.uk/jatos/742>`_

.. _forms:

Forms and Documentations
------------------------

NIBS Manual
Please contact the lab head to get access to the lab Manual

Safety report
:download:`Brain Stimulation Safety Report </brainstim/Brain_Stimulation_Safety_Report_v12.pdf>`

Safety questionnaire
:download:`TUS screening safety questionnaire </brainstim/BRIC_TUS_safety_questionnaire.pdf>`

Sample Information TUS
:download:`Sample information TUS </brainstim/Sample_information_TUS.pdf>`