.. BRIC-handbook documentation master file, created by
   sphinx-quickstart on Sat May 15 19:58:13 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=============
BRIC handbook
=============

.. contents:: Table of Contents

.. toctree::
   :maxdepth: 2
   :caption: BRIC general

   bric.rst
   bric/bricResources.rst

.. MRI Lab

.. toctree::
   :maxdepth: 2
   :caption: MRI Lab

   mri.rst
   mri/mrihowtos.rst
   mri/peripherals.rst

.. Brain Stimulation Lab

.. toctree::
   :maxdepth: 2
   :caption: Brain Stimulation Lab

   brainstim.rst
   brainstim/TUS.rst


.. Cognition & Behaviour Lab

.. toctree::
   :maxdepth: 2
   :caption: Cognition & Behaviour Lab

   cogbehav.rst

.. EEG Lab

.. toctree::
   :maxdepth: 2
   :caption: EEG Lab

   eeg.rst
   eeg/eeghowtos.rst

.. Motor Control Lab

.. toctree::
   :maxdepth: 2
   :caption: Motor Control Lab

   motor.rst

.. Psychopharmacology Lab

.. toctree::
   :maxdepth: 2
   :caption: Psychopharmacology Lab

   pharmaco.rst

.. Computational Modelling Lab

.. toctree::
   :maxdepth: 2
   :caption: Computational Modelling Lab

   modelling_azure.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

