==========================
Cognition & Behaviour Lab
==========================

Lab head: Alastair Smith

Lab resources
-------------
See the following documents for details of psychological assessments available within the lab.

:download:`Digital resoures - CANTAB <cogbehav/CANTAB.pdf>`

:download:`Physical resoures <cogbehav/PSYCHtests.pdf>`


Additional resources
--------------------
A number of additional psychometric tests are available to University of Plymouth staff and students. Information regarding this and how to access them can be found `here <https://www.psy.plymouth.ac.uk/PsychometricTests/default.aspx>`__
