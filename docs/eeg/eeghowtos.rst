================
EEG Lab - How to
================

Mobile EEG, [SMARTING] :download:`Link <https://files.mbraintrain.com/wp-content/uploads/2020/09/SMARTING-mobi-User-Manual.pdf>`

Wired EEG, [Brain Products] `internal link <https://gitlab.com/bric-plymouth/BRIChandbook/-/tree/master/docs/eeg/BRAIN_PRODUCTS>`__


How to use MOBI EEG
--------------------

.. Important::
    1. Only charge mobile EEG amplifier with provided black USB cable in a computer USB port, and that it is not plugged in to the cap. Otherwise it will not charge. 
    2. Light will be red when charging, turn green when charged.

STARTING UP
~~~~~~~~~~~~~~~
    1. Computer Pin: 2021
    2. Ensure that the mobile EEG is charged

SETTING UP CAP
~~~~~~~~~~~~~~~
    1. Soak the hydrolink sponges for 2 hours prior to use in saline solution.
    2. Insert sponge holder into electrode base, ensure click is heard, rotating clockwise.
    3. Once sponges are fully soaked, gently squeeze out extra saline solution, and insert into holder.
    4. Place the cap on participant’s head, and secure AROUND chin, not under. 
    5. Plug in amplifier (ensuring it is the correct way round – grove and protrusion line up, otherwise you will damage the pins). 
    6. Secure amplifier to cap. This is important for gyro to work properly.

`Link to setup video <https://youtu.be/FnQJOsagOYE/>`__

SETTING UP MOBI SYSTEM
~~~~~~~~~~~~~~~~~~~~~~
1. Double-click on ``Streamer3`` app (second purple ring on desktop). 
2. Click on :guilabel:`connect` (top left)
3. Select :guilabel:`port COM (5)`.
4. Rename EEG Stream (if doing multiple streams). 
5. Click :guilabel:`Connect`.

	Options

	On main screen (bottom right)
	    a) Select desired channel layout (if not using all channels, deselect :guilabel:`Select All Channels`, click on desired channels)
	    b) Choose whether display shows impedance values or electrode names.
	Under :guilabel:`Options` (right)
	    c) Keyboard markers: activates keyboard presses
	    d) Test signal: Check whether amp is streaming correctly. Does not need to be connected to cap. 
	    e) Gyro data: Select to include with EEG data
	    f) Impedance radio buttons: Impedance on ref will only measure impedance on reference electrode. Otherwise self- explanatory. 
	    g) Frequency: Select desired frequency. **Cannot measure impedance values at 250Hz**

.. Note::
	Once impedance has been checked, switch to “no impedance measurement” to reduce noise. 

6. Select :guilabel:`Start streaming signals`. 
7. Wait a few seconds, then select :guilabel:`Show Signals` to see the signal display window. 
    a) Time can be set to manual for custom timescale.
    b) Scale can also set to manual.
    c) Signal can be paused for inspection. This does not impact acquisition or recording.
8. **RECORDING**: Click button on right of pause button on ``Signal Display window``. 
    a) Select whether ``XDF`` (Lab Streaming Layer compatibility, can record multiple streams) or ``BDF`` (takes up less space, supported by more EEG processors)
    b) Select where to save file (either filepath or browse)
    c) Select :guilabel:`Start Recording`
9. On completion, select Disconnect to put the device in idle mode (and save battery!)

.. Note::
   If update option appears on top bar or on startup, ensure that the amplifier is updated. 