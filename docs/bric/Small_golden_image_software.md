- [x] Matlab + toolboxes https://uk.mathworks.com
- [x] Python3
- [ ] Rstudio -> excluded as school has Rstudio server

fMRI
- [x] SPM https://www.fil.ion.ucl.ac.uk/spm
- [x] SPM toolboxes: 
    - [x] CONN https://web.conn-toolbox.org/resources/installation
    - [x] marsbar https://sourceforge.net/projects/marsbar
    - [x] Artrepair https://cibsr.stanford.edu/tools/human-brain-project/
- [x] FSL  https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation/Linux
- [x] Freesurfer https://surfer.nmr.mgh.harvard.edu/fswiki//FS7_linux
- [x] MRIcronGL https://github.com/rordenlab/MRIcroGL
--> does not run on VMs withouth dedicated graphics card
- [ ] AFNI  --> excluded, too few users 
https://afni.nimh.nih.gov/pub/dist/doc/htmldoc/background_install/main_toc.html
- [ ] ANTs --> excluded, too few users
https://stnava.github.io/ANTs/
- [ ] Microdicom (Dicom viewer) -> only runs on windows
- [ ] 3DimViewer (Dicom viewer) --> needs graphics card
- [x] Amide http://amide.sourceforge.net/installation_linux.html

DTI
- [x] explore DTI http://www.exploredti.com
- [ ] Niftyreg https://github.com/KCL-BMEIS/niftyreg
- [ ] Niftyseg https://github.com/KCL-BMEIS/NiftySeg
- [x] MRtrix https://www.mrtrix.org/download/

MRS
- [x] Gannet 3.1 (matlab-based toolbox) http://www.gabamrs.com/downloads
- [x] FSL-MRS 
https://users.fmrib.ox.ac.uk/~saad/fsl_mrs/html/install.html
- [x] LCModel http://www.s-provencher.com/lcm-test.shtml

EEG
- [x] Fieldtrip  https://www.fieldtriptoolbox.org/ 
Matlab toolboxes required: https://www.fieldtriptoolbox.org/faq/requirements/ 

- [x] EEGlab https://sccn.ucsd.edu/eeglab/ressources.php 
- [x] ERPlab https://github.com/lucklab/erplab (installed a plugin within EEGLab)

- [ ] MNE
 https://mne.tools/stable/index.html  (Python-based)
 --> excluded, few users
 
- [x] Brainstorm 
https://neuroimage.usc.edu/brainstorm/Introduction (runs on Matlab, but they also provide executables that do not require a Matlab license).

TUS
- [ ] k-wave (matlab-based toolbox) http://www.k-wave.org/download.php (please also download the C++ simulation codes and unzip these in the binaries folder of the k-wave MATLAB toolbox)
--> excluded, too few users