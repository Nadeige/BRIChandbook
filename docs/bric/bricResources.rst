----------------------
BRIC resources
----------------------

Computing
----------

- Researchers involved in research projects at BRIC can be provided with a **Microsoft Azure Virtual Machine** for data analysis and other computing needs.

    - Virtual machines can have up to 8 CPUs, 32 Gb of memory and 12 GPUs (112 Gb of memory).
    - They come pre-installed with the most popular neuroimaging software (:download:`full list of pre-installed software </bric/Small_golden_image_software.md>`)
    - For more information, contact Paul Greening.
    - :ref:`Instructions for setting up and accessing your Azure VM <VMconcepts>`

- For analyses or simulations requiring more computing power, university staff can also request access to the university's **high-performance computing cluster(s)** (HPC).
    
    - For more information and to request an account, see `this page <https://www.plymouth.ac.uk/about-us/university-structure/faculties/science-engineering/hpc/what-is-a-cluster>`_
    - For help on using the University's HPC, see `this page <https://ecm-research.plymouth.ac.uk/hpc/foseres-uop/>`_
    
- The school of psychology owns a multi-user 64-thread Linux workstation with 2 GPUs, DeepSim, managed by Andy Wills.
    
    - For more information and request access, see `this page <https://www.andywills.info/deepsim/>`_


.. include:: /bric/azure/Overview.rst
.. include:: /bric/azure/Windows.rst
.. include:: /bric/azure/Linux.rst
.. include:: /bric/azure/MacOS.rst
.. include:: /bric/azure/Storage.rst
.. include:: /bric/azure/General-Info.rst
.. include:: /bric/azure/Links.rst
.. include:: /bric/azure/Labs.rst