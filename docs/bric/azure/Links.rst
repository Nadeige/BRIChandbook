Links to other useful resources
-------------------------------

A basic introduction to the `Linux Commandline <https://ubuntu.com/tutorials/command-line-for-beginners#1-overview>`_

An introduction to `Environment and PATH variables <https://help.ubuntu.com/community/EnvironmentVariables>`_ in Ubuntu.

Azure Portal `login <https://login.microsoftonline.com/>`_


