LAB INFO HERE

Azure LAB user guide
--------------------

This wiki outlines to steps needed to register, log-in and manage your UoP BRIC student Azure LAB virtual machine. It is very important to read and follow the logout activities detailed at the end. Should you have any questions please contact Paul Greening paul.greening@plymouth.ac.uk

Register for LAB
****************

Your invite to register for an AZURE lab will be via email from Microsoft Azure,

.. image:: /bric/azure/Images/LAB-img-1.png
  :width: 600
  :align: center
  :alt: LAB INVITE 1

The email will contain a link to register for the lab,

.. image:: /bric/azure/Images/LAB-img-2.png
  :width: 600
  :align: center
  :alt: LAB INVITE 2

Click the link and use your UoP credentials to log in,

.. image:: /bric/azure/Images/LAB-img-3.png
  :width: 600
  :align: center
  :alt: LAB INVITE 3

It will take a few seconds to register,

.. image:: /bric/azure/Images/LAB-img-4.png
  :width: 600
  :align: center
  :alt: LAB INVITE 4

Eventually you will be logged into Azure Lab Services and see the Virtual Machine (VM) you have been assigned,

.. image:: /bric/azure/Images/LAB-img-5.png
  :width: 600
  :align: center
  :alt: LAB INVITE 5

.. note::
  You can return to your Azure lab at any time using this url `My Virtul Machine - Azure Lab Services <https://labs.azure.com/virtualmachines>`_



Start your LAB VM
*****************

To start the VM use the toggle switch to begin booting,

.. image:: /bric/azure/Images/LAB-img-6.png
  :width: 600
  :align: center
  :alt: VM Start 1

It will take a couple of minutes before becoming available,

.. image:: /bric/azure/Images/LAB-img-7.png
  :width: 600
  :align: center
  :alt: VM Start 2

Connect to your VM
******************

To connect to your VM, click on the monitor icon,

.. image:: /bric/azure/Images/LAB-img-8.png
  :width: 600
  :align: center
  :alt: Connect VM 1


This provides two options,
•	RDP - for a GUI desktop environment login
•	SSH -  for command line only login

Select RDP, this will download a Remote Desktop Connection file

.. image:: /bric/azure/Images/LAB-img-9.png
  :width: 600
  :align: center
  :alt: Connect VM 2

Open the downloaded file to establish an RDP connection to your VM,

.. image:: /bric/azure/Images/LAB-img-10.png
  :width: 600
  :align: center
  :alt: Connect VM 3

Just click the OK button.

You are now logged into your VM, the desktop should look like this,

.. image:: /bric/azure/Images/LAB-img-11.png
  :width: 600
  :align: center
  :alt: Connect VM 4

Your VM is running a linux (Ubuntu) operating system. Most activities will be through GUI menus, however as some initial user setup is needed, a basic knowledge of linux commands would be useful. The initial steps to connect your university OneDrive account are detailed below.

Connect your OneDrive account
*****************************

Open Onedriver, which is accessible from the application menu, under accessories,

.. image:: /bric/azure/Images/LAB-img-12.png
  :width: 600
  :align: center
  :alt: OneDrive 1

Open it, below is how it should look,

.. image:: /bric/azure/Images/LAB-img-13.png
  :width: 600
  :align: center
  :alt: OneDrive 2

Click the + button in the top left of the Onedriver window, and
select the mountpoint.

The mountpoint is /home/lab1user/Desktop/Onedrive, see
example below:

.. image:: /bric/azure/Images/LAB-img-14.png
  :width: 600
  :align: center
  :alt: OneDrive 3

Select the mountpoint, and you will be prompted to enter your UoP
credentials. Accept the permissions.

.. image:: /bric/azure/Images/LAB-img-15.png
  :width: 600
  :align: center
  :alt: OneDrive 4

Once you enter your login and password it will sync to your Onedrive account, which subsequently will be accessible
via the Onedriver folder on your desktop.

.. image:: /bric/azure/Images/LAB-img-16.png
  :width: 600
  :align: center
  :alt: OneDrive 5

## YOU ARE NOW READY TO START USING THE LAB AS GUIDED BY YOUR TUTOR ##


Stop your LAB VM
****************

IMPORTANT logout & shutdown instructions – please read.

When your work is complete it is essential to logout of the VM and then shut it down. This will ensure you do not unnecessarily waste your limited hours for this lab. Please follow these steps,

Click on your username (lab1user) in the top right of the session, 

.. image:: /bric/azure/Images/LAB-img-17.png
  :width: 600
  :align: center
  :alt: Logout 1

Select Log Out,

.. image:: /bric/azure/Images/LAB-img-18.png
  :width: 600
  :align: center
  :alt: Logout 2

Back on the Azure LAB services web page, you will see the VM is still running,

.. image:: /bric/azure/Images/LAB-img-19.png
  :width: 600
  :align: center
  :alt: Logout 3

It is essential the VM is shutdown using the toggle switch,

.. image:: /bric/azure/Images/LAB-img-20.png
  :width: 600
  :align: center
  :alt: Logout 4

.. image:: /bric/azure/Images/LAB-img-21.png
  :width: 600
  :align: center
  :alt: Logout 5

URL for your LAB
****************

Once registered you can return to your Azure lab at any time using this url `My Virtul Machine - Azure Lab Services <https://labs.azure.com/virtualmachines>`_

