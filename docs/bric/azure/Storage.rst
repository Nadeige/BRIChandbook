Connect and Access Storage
--------------------------

Access Shared Storage on a VM
*****************************

Command Line
............
Based on your defined requirements there will already be a connection to your shared storage account on your VM. From the VM command prompt the df -h command will list filesystem, we are interested in the remote ones which begin with //sauopbric????? as in the example below.

.. image:: /bric/azure/Images/Storage-1.png
  :width: 700 px
  :align: center
  :alt: Storage Explorer 1

In the first column “Filesystem”, you will see the storage account name and where it can be accessed in the “Mounted on” column.

In the example above we see,


 /media/HP-LloH-HV-010622   ( RAW DATA - from scanner in repository REPO0 )

 /media/STUDY-HP-LloH-HV-010622   ( STUDY DATA - shared with all involved in study )

 /media/MY-DATA   ( PERSONAL DATA - personal storage area per researcher )

 /media/COMMON-DATA   ( COMMON DATA - available to all for sharing files, documents, data, etc. )  


Desktop
.......





Access Shared Storage remotely
******************************

Microsoft Storage Explorer is free to install on Windows/Mac/Linux from this `link <https://azure.microsoft.com/en-gb/products/storage/storage-explorer/>`_

Once installed start the app and add your UoP account, by clicking "Sign in with Azure"

.. image:: /bric/azure/Images/Storage-2.png
  :width: 600 px
  :align: center
  :alt: Storage Explorer 2


You will need to select Azure,

.. image:: /bric/azure/Images/Storage-4.png
  :width: 600 px
  :align: center
  :alt: Storage Explorer 4


Then select the BRIC subscription,

.. image:: /bric/azure/Images/Storage-3.png
  :width: 600 px
  :align: center
  :alt: Storage Explorer 3


Enter your UoP credentials when prompted. Once complete you should see something like below. Note you may have access to more than one subscription. We are interested only in UoP – Brain Research Imaging Centre, untick all others that appear.

.. image:: /bric/azure/Images/Storage-4a.png
  :width: 600 px
  :align: center
  :alt: Storage Explorer 4a


Now click on the top icon on the sidebar, navigate to your storage account and the File Shares, from where your will see the data held within your share, and also options to upload or download data

.. image:: /bric/azure/Images/Storage-5.png
  :width: 600 px
  :align: center
  :alt: Storage Explorer 5


Connect UoP OneDrive to VM
**************************

It is possible to connect your UoP OneDrive account to your BRIC VM, however without being selective about which folders are synchronised, it highly likely your VM will not have enough internal disk space to hold all of the data. Below are the steps needed to connect and select which folders are to be available on your VM.

Onedrive should already be installed on your VM, to confirm::

  which onedrive

this will show the location of the installed program. ( expect to see /usr/bin/ondrive )

Before connecting configure which folders in your UoP OneDrive account are to be synchronised. Update entries in file:: 

  /home/Researcher/.config/onedrive/sync_list

Which looks like this::

  # sync_list supports comments
  #
  # The ordering of entries is highly recommended - exclusions before inclusions
  #
  # Exclude everything
  !/Your-Foldername1/*
  !/Your-Foldername2/*
  #
  # Exclude secret data folder in root directory only
  !/Secret_data/*
  #
  # Include just the folders you want to see on your VM
  /Your-Foldername3/*
  /Your-Foldername4/sub-foldername/*
  #

Now start and connect to onedrive::

  onedrive

Which will prompt you for authorisation. Click the url, 


.. image:: /bric/azure/Images/Onedrive-1.png
  :width: 700 px
  :align: center
  :alt: OneDrive 1

enter your UoP credentials and then copy the resultant url from your browser,

.. image:: /bric/azure/Images/Onedrive-1a.png
  :width: 700 px
  :align: center
  :alt: OneDrive 1a

Paste the url back into the waiting command

.. image:: /bric/azure/Images/Onedrive-2.png
  :width: 700 px
  :align: center
  :alt: OneDrive 2

Now check on the sync status::

  onedrive --display-sync-status

When you initially setup OneDrive it will take some time to fully synchronise, but generally will be regularly updaing in the background and should look like this,

.. image:: /bric/azure/Images/Onedrive-3.png
  :width: 700 px
  :align: center
  :alt: OneDrive 3

From the command line on your VM OneDrive files are found under /home/Researcher/OneDrive,

.. image:: /bric/azure/Images/Onedrive-4.png
  :width: 700 px
  :align: center
  :alt: OneDrive 4


From the GUI there is a desktop link,

.. image:: /bric/azure/Images/Onedrive-5.png
  :width: 100 px
  :align: center
  :alt: OneDrive 5
  

Connect Azure Storage to Mac
****************************

In addition to using Azure Storage Explorer as described in the previuos section, it is possible to directly connect an Azure Storage account to a Mac. The extract below is taken from `here <https://learn.microsoft.com/en-us/azure/storage/files/storage-how-to-use-files-mac>`_ 


Open Finder - Finder is open on macOS by default, but you can ensure that it's the currently selected application by clicking the macOS face icon on the dock

.. image:: /bric/azure/Images/Stg-mac-1.png
  :width: 100 px
  :align: center
  :alt: StgMac-1

Select "Connect to Server" from the "Go" Menu: Using the UNC path, convert the beginning double backslash (\\) to smb:// and all other backslashes (\) to forward slashes (/). Your link should look like the following

.. image:: /bric/azure/Images/Stg-mac-2.png
  :width: 700 px
  :align: center
  :alt: StgMac-2

Use the storage account name and storage account key, (available from BRIC IT support), when prompted for a username and password: When you select Connect on the Connect to Server dialog, you'll be prompted for the username and password (this will be autopopulated with your macOS username). You have the option of placing the storage account name/storage account key in your macOS Keychain.

.. note:: 

  Storage Account Keys can be requested from `BRIC IT Support  <mailto:paul.greening@plymouth.ac.uk>`_. 


Use the Azure file share as desired: After substituting the share name and storage account key for the username and password, the share will be mounted. You may use this as you would normally use a local folder/file share, including dragging and dropping files into the file share

.. image:: /bric/azure/Images/Stg-mac-3.png
  :width: 700 px
  :align: center
  :alt: StgMac-3











