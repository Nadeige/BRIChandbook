Managing your VM on Linux
-------------------------

Start VM from Web browser
*****************************

Login to the `Microsoft ARM Portal <https://portal.azure.com/#home>`_ using your preferred web browser. The credentials required will be you UoP id and password.
Once logged in, select "virtual machines" from the left hand column, you will then see your VM listed, from where it can be started/stopped

.. image:: /bric/azure/Images/ARM-Portal.png
  :width: 700
  :align: center
  :alt: Azure VMs

Note the VM status of "Stopped (deallocated)", this is always the required status when your VM is not being used.

Check the tick-box next to your VM name, then use the start or stop icon from the options across the top of the page.
When your VM status changes to running, it may take a further minute for the network to initialise before allowing remote connections.


Start VM from Linux Command Line
********************************

Pre-requisites
..............

Installing the Linux Azure Client is a pre-requisite to being able to manage VMs from the linux command line. Details of the installation for linux can be found `here <https://learn.microsoft.com/en-us-cli/azure/>`_. 


Connect to your Azure Account
*****************************


Connect using this command, you will be prompted to enter your UoP credentials::

  az login

This will open a browser window for you to enter your userid and password. You may also need to completed two factor authentication (2FA).

.. image:: /bric/azure/Images/Connect-AzAccount.PNG
  :width: 300
  :align: center
  :alt: Connect AZ Account


If you are connected remotely to a linux machine or working on one that does not have a desktop environment, you will get a prompt like this,


.. image:: /bric/azure/Images/Linux-cmd-1.png
  :width: 500 px
  :align: center
  :alt: Linux-cmd-1


Open a web browser and enter the code provided,


.. image:: /bric/azure/Images/Linux-cmd-2.png
  :width: 300 px
  :align: center
  :alt: PIC2


You will then be prompted to enter your UoP credentials,

.. image:: /bric/azure/Images/Linux-cmd-3.png
  :width: 300
  :align: center
  :alt: Linux-cmd-3


Once login is successful,

.. image:: /bric/azure/Images/Linux-cmd-4.png
  :width: 300
  :align: center
  :alt: Linux-cmd-4


You can then return to you command prompt where you will be connected,


.. image:: /bric/azure/Images/Linux-cmd-5.png
  :width: 500
  :align: center
  :alt: Linux-cmd-5


You may have access to other Azure subscriptions which will be listed at login. You can list your subscriptions with::

  az account subscription list 

Using the output from the command above you can set the BRIC subscription::

  az account set -s xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx

After which you can confirm which account you are connected to with::

  az account show

Start Your VM
*************

Start your VM with::

  az vm start --resource-group YYYYYYYY --name XXXXXXXX

where YYYYYYYY is the resource group of your VM and XXXXXXXX is the name of your VM. Note the double hyphen preceding resource-group and name.

Stop and deallocate your VM
***************************

To stop and deallocate your VM::

  az vm deallocate --resource-group YYYYYYYY --name XXXXXXXX

Connect to your VM - Command Line
*********************************

From the linux command line using SSH::

  ssh researcher@your.vm.ip.address

From a terminal emulator such as PuTTy, which is available for install from the software repository or from a command line::

  sudo apt-get install putty 

Once installed open the PuTTy application and configure connection setting as needed.

Connect to your VM – Desktop Environment
****************************************

Firstly, you need a terminal emulator installed, in this example remmina is used.
Either install from the software repository or from the linux terminal window, run these commands::

  sudo apt-add-repository ppa:remmina-ppa-team/remmina-next
  sudo apt update
  sudo apt install remmina remmina-plugin-rdp remmina-plugin-secret

Once installed, open the emulator, which is this icon, 

.. image:: /bric/azure/Images/Remmina.png
  :width: 100
  :align: center
  :alt: Remmina
 

 
and configure the connection settings.




