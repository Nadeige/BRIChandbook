Automation
----------

Shutdown of VM
**************

On your Research VM running script /home/Researcher/Shutdown-Deallocate-VM.bsh will shutdown and deallocate your VM in Azure. The script must be run as user Researcher.

It can be run manually or added to the end of your script. It will take a couple of minutes to be processed.


Command Summary
***************

Examples below are using rg-bric-admin as the resource-group name and vm-bric-devtest-1 as the virtual machine name. Replace these with your resource-group and virtual machine names.


.. list-table:: Windows
   :widths: 50 150
   :header-rows: 1

   * - Activity
     - Command
   * - Login to Azure
     - Connect-AzAccount
   * - List your Azure Subscriptions
     - Get-AzSubscription
   * - Set to BRIC Azure Subscription
     - Select-AzSubscription -SubscriptionId 'xxxxxxxxxxxxx'
   * - Identify your resource group name
     - Get-AzVM -Name vm-bric-devtest-1
   * - Start Virtual Machine
     - Start-AzVM -Name vm-bric-devtest-1 -ResourceGroupName rg-bric-admin
   * - Stop & deallocate Virtual Machine
     - Stop-AzVM -Name vm-bric-devtest-1 -ResourceGroupName rg-bric-admin -Force


.. list-table:: Linux
   :widths: 50 150
   :header-rows: 1

   * - Activity
     - Command
   * - Login to Azure
     - az login
   * - List your Azure Subscriptions
     - az account subscription list
   * - Set to BRIC Azure Subscription
     - az account set -s  'xxxxxxxxxxxxx'
   * - Identify your resource group name
     - az vm list | grep -i resourcegroup\": | tail -1
   * - Start Virtual Machine
     - az vm start --resource-group rg-bric-admin --name vm-bric-devtest-1
   * - Stop & deallocate Virtual Machine
     - az vm deallocate --resource-group rg-bric-admin --name vm-bric-devtest-1


.. list-table:: MacOS
   :widths: 50 150
   :header-rows: 1

   * - Activity
     - Command
   * - Login to Azure
     - az login
   * - List your Azure Subscriptions
     - az account show
   * - Set to BRIC Azure Subscription
     - az account set -s  'xxxxxxxxxxxxx'
   * - Identify your resource group name
     - az vm list | grep -i resourcegroup\": | tail -1
   * - Start Virtual Machine
     - az vm start --resource-group rg-bric-admin --name vm-bric-devtest-1
   * - Stop & deallocate Virtual Machine
     - az vm deallocate --resource-group rg-bric-admin --name vm-bric-devtest-1

|
