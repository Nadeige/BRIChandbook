Managing your VM on Windows
---------------------------

Start/Stop from a Web browser
*****************************

Login to the `Microsoft ARM Portal <https://portal.azure.com/#home>`_ using your preferred web browser. The credentials required will be you UoP id and password.
Once logged in, select "virtual machines" from the left hand column, you will then see your VM listed, from where it can be started/stopped

.. image:: /bric/azure/Images/ARM-Portal.png
 :width: 700 px
 :align: center
 :alt: Azure VMs Image 1 PNG
 

.. note::
  The VM status of "Stopped (deallocated)", is always the required status when your VM is not being used.Check the tick-box next to your VM name, then use the start or stop icon from the options across the top of the page. When your VM status changes to running, it may take a further minute for the network to initialise before allowing remote connections.



Start/Stop from a command line prompt
*************************************

Pre-requisites
..............
There are two pre-requisites to enable managing your VM from the Windows command line,

1. Windows Powershell modules. (Sould come as part of the standard TIS desktop).
2. Azure Powershell modules. (install guide `here <https://learn.microsoft.com/en-us/powershell/azure/install-azure-powershell?view=azps-12.0.0&viewFallbackFrom=azps-6.6.0>`_)

Connect to your Azure Account.
..............................

With the pre-requisites in place you need to connect/authenticate your account with azure. 
Open a Powershell terminal, and run command::

  Connect-AzAccount

.. image:: /bric/azure/Images/PowerShell.PNG
 :width: 400
 :align: center  
 :alt: Powershell Login


This will then open a browser window for you to enter your userid and password. 
You may also need to completed two factor authentication (2FA).

.. image:: /bric/azure/Images/Connect-AzAccount.PNG
  :width: 300
  :align: center
  :alt: Connect AZ Account


Once authentication is complete, back on the console window you will see the subscription you are now connected to. You may have multiple Azure subscriptions, so ensure it is the Brain Research Imaging Centre subscription that is showing.
To set the default subscription to BRIC::

  Select-AzSubscription -SubscriptionId 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
     
Where xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx is the BRIC SubscriptionId.
To list available subscriptions::

  Get-AzSubscription

This will give you the Id of the BRIC subscription to use if needed.

Start your VM
.............
 
Now you can start your VM with the following command syntax::

  Start-AzVM -Name XXXXXXXXX -ResourceGroupName YYYYYYYYY
 
Where XXXXXXXXX is the name of your VM and YYYYYYYYY is the Azure Resource Group it belongs to.

Stop and deallocate your VM
...........................

To stop your VM::

  Stop-AzVM -Name XXXXXXXXX -ResourceGroupName YYYYYYYYY

Where XXXXXXXXX is the name of your VM and YYYYYYYYY is the Azure Resource Group it belongs to.

Query status of your VM
.......................

To query the status of your VM::

 Get-AzVM -Name XXXXXXXXX -ResourceGroupName YYYYYYYYY
 
Where XXXXXXXXX is the name of your VM and YYYYYYYYY is the Azure Resource Group it belongs to.
Example output from these commands,

.. image:: /bric/azure/Images/Powershell-5.PNG
  :width: 200
  :align: center
  :alt: Powershell examples
  

Connect to your VM - Command line
*********************************

None of the Research VMs are in DNS so access will require knowledge of the correct IP address.
Access is via secure shell (SSH) connections. There are two options, either direct from the windows command line or via a 3rd party terminal emulator.

From a windows command line (not PowerShell)
............................................
::

  ssh -l Researcher www.xxx.yyy.zzz
  
Where www.xxx.yyy.zzz is the ipaddress of your VM

The first time you connect respond Y to the prompt confirming the connection.
You now have a terminal open to your linux research VM. A basic knowledge of linux commands and the vi editor would be beneficial before using the command line. Regular users of the command line should consider the PuTTy terminal emulator.

From a windows Terminal Emulator (PuTTy)
........................................
There are many terminal emulators available for this purpose. My preference is PuTTy details of which are available from the University `Work at Home <https://liveplymouthac.sharepoint.com/sites/WorkAtHome/SitePages/Freeware.aspx>`_ sharepoint site.
Once Putty is installed you can save connection settings and customisations of the terminal session. You are also able to create and save access keys to allow password less access from your device. 

Connect to your VM – Desktop Environment
****************************************

Access to the desktop environment is via Remote Desktop Protocol (RDP).
RDP provides a GUI desktop environment for running commands and applications that have a graphical interface, i.e. MatLab. Open the Remote Desktop Connection App, and enter your VM ip address,
 
.. image:: /bric/azure/Images/RDP-1.PNG
  :width: 300
  :align: center
  :alt: RDP example

Which will prompt you to confirm you want to proceed,

.. image:: /bric/azure/Images/RDP-2a.png
  :width: 300
  :align: center
  :alt: RDP example


You will then be prompted for your VM username and password  (Not your UoP username and password),

.. image:: /bric/azure/Images/RDP-3.PNG
  :width: 200
  :align: center
  :alt: RDP example
                          